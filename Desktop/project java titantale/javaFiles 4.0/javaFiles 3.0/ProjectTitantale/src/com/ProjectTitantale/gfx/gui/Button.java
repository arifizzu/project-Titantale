package com.ProjectTitantale.gfx.gui;

import com.ProjectTitantale.Game;

import java.awt.*;

public class Button {

    public int x, y;
    public int width, height;
    public int titleScreenState;

    public String label;

    public Button(int x, int y, int width, int height, String label) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.label = label;
    }

    public void render(Graphics g){


        g.setFont(new Font("arial", Font.BOLD, 70));
        g.setColor(Color.WHITE);
        g.drawString("TITANTALE ADVENTURE", Game.WIDTH - 130, 150);
        g.setFont(new Font("arial", Font.BOLD, 30));
        g.setColor(Color.RED);
        g.drawString( "Controls", Game.WIDTH/2+200, 410);
        g.setColor(Color.white);
        g.setFont(new Font("arial", Font.BOLD, 20));
        g.drawString("W        -->      Up", Game.WIDTH/2+230, 460);
        g.drawString("S         -->      Down", Game.WIDTH/2+230, 490);
        g.drawString("A         -->      Left", Game.WIDTH/2+230, 520);
        g.drawString("D         -->      Right", Game.WIDTH/2+230, 550);
        g.drawString("E         -->      Ice", Game.WIDTH/2+230, 580);
        g.drawString("Space    -->      Fire", Game.WIDTH/2+210, 610);
        g.setColor(Color.WHITE);
        g.setFont(new Font("arial", Font.BOLD, 30)); // font size of main menu text
        g.drawRect(getX(), getY(), getWidth(), getHeight());

        FontMetrics fm = g.getFontMetrics();
        int stringX = (getWidth() - fm.stringWidth(getLabel())) / 2; //set text to center
        int stringY = (fm.getAscent() + (getHeight() - (fm.getAscent() + fm.getDescent())) / 2);
        g.drawString(getLabel(), getX() + stringX, getY() + stringY);



    }




    public void triggerEvent(){

        if(getLabel().toLowerCase().contains("start")) Game.playing = true;
        else if (getLabel().toLowerCase().contains("exit")) System.exit(0);
        else if(getLabel().toLowerCase().contains("how")) titleScreenState = 1;

    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
