package com.ProjectTitantale.gfx.gui;

import com.ProjectTitantale.Game;

import java.awt.*;



public class Launcher {

    public int titleScreenState;

    public Button[] buttons;

    public Launcher(){
        buttons = new Button[2];

        buttons[0] = new Button(440, Game.getFrameHeight()/2-170, Game.getFrameWidth()-850,50,"Start Game");
        buttons[1] = new Button(440, Game.getFrameHeight()/2-90, Game.getFrameWidth()-850,50,"Exit Game");

    }

    public void render(Graphics g) {

        g.setColor(Color.BLACK);
        g.fillRect(0, 0, Game.getFrameWidth(), Game.getFrameHeight());

        for (int i = 0; i < buttons.length; i++) {
            buttons[i].render(g);
        }


    }
}
